import './bootstrap';
import 'vuetify/styles'
import { createApp } from 'vue/dist/vue.esm-bundler';
import { createVuetify } from 'vuetify'
import { createRouter, createWebHashHistory} from 'vue-router'
import App from './App.vue'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import Counter from '../components/Counter.vue'
const vuetify = createVuetify({
  components,
  directives,
})


const Home = {template: '<div>home</div>'}
const About = {template: '<div>about</div>'}

const routes = [
    {path:'/', component:Home},
    {path:'/about', component:Counter},

]
const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
  })


const app = createApp(App)
app.use(vuetify)
app.use(router)

app.mount('#app')